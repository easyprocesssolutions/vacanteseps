from django.db import models


class Valores(models.Model):
    name = models.CharField(max_length=100)
    aprobado = models.BooleanField()

    def __str__(self):
        return self.name
