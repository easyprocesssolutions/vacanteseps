from django.views.generic import ListView
from test_autos.parameters.models import Valores
from django.views.generic.edit import CreateView
from django.views.generic.edit import UpdateView


class ListValores(ListView):
    template_name = 'parameters/list.html'
    model = Valores


class CreateValores(CreateView):
    template_name = 'parameters/create.html'
    model = Valores
    fields = ['name', 'aprobado']
    success_url = 'ListValores'


class UpdateValores(UpdateView):
    template_name = 'parameters/update.html'
    model = Valores
    fields = ['name', 'aprobado']
    success_url = '/ListValores'
