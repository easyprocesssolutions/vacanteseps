from django.conf.urls import url
from .views import ListValores
from .views import CreateValores
from .views import UpdateValores


urlpatterns = [
                url(r'^ListValores$', ListValores.as_view(), name='listvalores'),
                url(r'^CreateValores$', CreateValores.as_view(), name='createvalores'),
                url(r'^UpdateValores/(?P<pk>[0-9]+)$', UpdateValores.as_view(), name='updatevalores'),

              ]