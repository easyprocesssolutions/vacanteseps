from django.conf.urls import url
from .views import ListAutoTest
from .views import CreateAutoTest



urlpatterns = [
                url(r'^ListAutoTest$', ListAutoTest.as_view(), name='listautotest'),
                url(r'^CreateAutoTest$', CreateAutoTest.as_view(), name='createautotest'),
              ]