from django.views.generic import ListView
from extra_views import CreateWithInlinesView, InlineFormSet
from .models import AutoTest
from .models import DetailTest


class ListAutoTest(ListView):
    template_name = 'auto_test/list.html'
    model = AutoTest


class ItemInline(InlineFormSet):
    model = DetailTest
    fields = '__all__'


class CreateAutoTest(CreateWithInlinesView):
    template_name = 'auto_test/create.html'
    model = AutoTest
    inlines = ItemInline
    fields = '__all__'
    success_url = 'ListAutoTest'



