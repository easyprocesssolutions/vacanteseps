from django.db import models


class AutoTest(models.Model):
    auto_name = models.ForeignKey('auto.Auto', on_delete=models.CASCADE)


class DetailTest(models.Model):
    relationship = models.ForeignKey(AutoTest, on_delete=models.CASCADE)
    test = models.ForeignKey('unique_test.UniqueTest', on_delete=models.CASCADE)
    parameter = models.ForeignKey('parameters.Valores', on_delete=models.CASCADE)
