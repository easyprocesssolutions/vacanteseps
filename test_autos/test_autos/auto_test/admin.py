from django.contrib import admin
from .models import AutoTest
from .models import DetailTest


class DetailTestInline(admin.TabularInline):
    model = DetailTest


class AutoTestAdmin(admin.ModelAdmin):
    inlines = [DetailTestInline]

admin.site.register(AutoTest, AutoTestAdmin)
