from django.views.generic import ListView
from test_autos.auto.models import Auto
from django.views.generic.edit import CreateView
from django.views.generic.edit import UpdateView


class ListAuto(ListView):
    template_name = 'auto/list.html'
    model = Auto


class CreateAuto(CreateView):
    template_name = 'auto/create.html'
    model = Auto
    fields = ['name', 'modelo', 'placa']
    success_url = 'ListAuto'


class UpdateAuto(UpdateView):
    template_name = 'auto/update.html'
    model = Auto
    fields = ['name', 'modelo', 'placa']
    success_url = '/ListAuto'


