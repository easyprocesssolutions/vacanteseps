from django.conf.urls import url
from .views import ListAuto
from .views import CreateAuto
from .views import UpdateAuto


urlpatterns = [
                url(r'^ListAuto$', ListAuto.as_view(), name='listauto'),
                url(r'^CreateAuto$', CreateAuto.as_view(), name='createauto'),
                url(r'^UpdateAuto/(?P<pk>[0-9]+)$', UpdateAuto.as_view(), name='updateauto'),

              ]