from django.views.generic import ListView
from test_autos.unique_test.models import UniqueTest
from django.views.generic.edit import CreateView
from django.views.generic.edit import UpdateView


class ListUniqueTest(ListView):
    template_name = 'unique_test/list.html'
    model = UniqueTest


class CreateUniqueTest(CreateView):
    template_name = 'unique_test/create.html'
    model = UniqueTest
    fields = ['name']
    success_url = 'ListUniqueTest'


class UpdateUniqueTest(UpdateView):
    template_name = 'unique_test/update.html'
    model = UniqueTest
    fields = ['name']
    success_url = '/ListUniqueTest'