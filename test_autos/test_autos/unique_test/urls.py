from django.conf.urls import url
from .views import ListUniqueTest
from .views import CreateUniqueTest
from .views import UpdateUniqueTest


urlpatterns = [
                url(r'^ListUniqueTest$', ListUniqueTest.as_view(), name='listuniquetest'),
                url(r'^CreateUniqueTest$', CreateUniqueTest.as_view(), name='createuniquetest'),
                url(r'^UpdateUniqueTest/(?P<pk>[0-9]+)$', UpdateUniqueTest.as_view(), name='updateuniquetest'),

              ]