from django.apps import AppConfig


class UniqueTestConfig(AppConfig):
    name = 'unique_test'
